package legislabrasil.communication

import java.time.ZonedDateTime


case class Project(val projectID: ProjectID,val owner: UserID, val name:String, val shortDescription:String, val creationDate:ZonedDateTime)
{

}
