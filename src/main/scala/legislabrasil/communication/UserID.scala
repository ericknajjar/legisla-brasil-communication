package legislabrasil.communication

case class UserID(rawId:String)
{
  override def toString(): String = rawId
}