package legislabrasil.communication

case class ProjectID(rawId:String)
{
  override def toString(): String = rawId
}
